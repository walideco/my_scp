package scp;

import static java.lang.System.identityHashCode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.tangosol.io.nio.ByteBufferInputStream;

public class Server implements Runnable {

  public final static Object LOCK = new Object();

  private String hostName;
  private char[] password;
  private ServerSocketChannel socket;
  private SocketChannel socketChannel;
  private Selector selector;
  private boolean stop;
  private KeyStore ks;
  private KeyPair key;
  private Map<Integer, Key> sessionKeys;

  public Server(String hostName, String pwd, int port) {
    this.hostName = hostName; // **** mail c est meiux
    this.password = pwd.toCharArray();
    sessionKeys = new HashMap<>();
    Security.addProvider(new BouncyCastleProvider());
    OutputStream os;
    try {
      this.ks = KeyStore.getInstance("JKS");
      InputStream is = new FileInputStream(new File("PublicKS.cer"));
      ks.load(is, "".toCharArray());
      is.close();
      os = new FileOutputStream(hostName + "/ks_" + hostName + ".cer");
      ks.store(os, this.password);
      os.close();
      socket = ServerSocketChannel.open();
      socket.bind(new InetSocketAddress(port));
      socket.configureBlocking(false);
      selector = Selector.open();
      socket.register(this.selector, SelectionKey.OP_ACCEPT);
      KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA", "BC");
      key = keyGenerator.generateKeyPair();

    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException
        | NoSuchProviderException e) {
      System.err.println("Impossible de demarrer les client " + hostName);
      e.printStackTrace();
      System.exit(0);
    }
    stop = false;
  }

  public void accept() {
    SocketChannel sc = null;
    try {
      sc = socket.accept();
      if (sc != null) {
        System.out.println("Nouveau client :  " + sc);
        sc.configureBlocking(false);
        sc.register(this.selector, (SelectionKey.OP_READ));
      }
    } catch (IOException e) {
      System.err.println("Impossible d'accepter le client");
      e.printStackTrace();
    }
  }

  public void registerDemande() throws FileNotFoundException, IOException, NoSuchAlgorithmException,
      NoSuchProviderException, InvalidKeySpecException {
    synchronized (LOCK) {
      System.out.println("Enregistrement de demande de certificat aupres du CA...");
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("certRequests.txt")));

      KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
      RSAPublicKeySpec rsaPublicKeySpec = kf.getKeySpec(key.getPublic(), RSAPublicKeySpec.class);

      oos.writeUTF(hostName);
      oos.writeObject(rsaPublicKeySpec.getModulus());
      oos.writeObject(rsaPublicKeySpec.getPublicExponent());

      oos.close();
    }
  }

  public void getCertificates(String adresseCAserver, int CAPortt)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException,
      ClassNotFoundException, CertificateException, KeyStoreException {

    InetSocketAddress is = new InetSocketAddress(adresseCAserver, CAPortt);
    this.socketChannel = SocketChannel.open();
    System.out.println("Attente de certificat...");
    if (!this.socketChannel.connect(is))
      throw new ConnectException("CA non disponnible");

    System.out.println("Réception ... ");
    InputStream in = socketChannel.socket().getInputStream();
    CertificateFactory cf = CertificateFactory.getInstance("X.509");
    boolean hasnextCert = true;
    do {
      try {
        X509Certificate certificate = (X509Certificate) cf.generateCertificate(in);
        // System.out.println(certificate);
        if (!this.verifyCertifivate(certificate))
          throw new CertificateException("Impossible de verifier le certificat");
        if (certificate.getSubjectDN().getName().equals("CN=" + hostName)) {
          Certificate[] tmp = {certificate};
          ks.setKeyEntry(certificate.getSubjectDN().getName(), key.getPrivate(), this.password, tmp);
        } else
          ks.setCertificateEntry(certificate.getSubjectDN().getName(), certificate);
      } catch (CertificateException e) {
        hasnextCert = false;
      }
    } while (hasnextCert);

    OutputStream os = new FileOutputStream(hostName + "/ks_" + hostName + ".cer");
    ks.store(os, password);
    socketChannel.close();
  }

  public boolean verifyCertifivate(Certificate certificate) {
    if (certificate == null)
      return false;
    try {
      Certificate certCA = ks.getCertificate("cacertificate");
      certificate.verify(certCA.getPublicKey());

    } catch (KeyStoreException | InvalidKeyException | CertificateException | NoSuchAlgorithmException
        | NoSuchProviderException | SignatureException e) {
      return false;
    }
    return true;
  }

  public String authentificate(SelectionKey s)
      throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException {
    SocketChannel sc = (SocketChannel) s.channel();

    ByteBuffer bb = ByteBuffer.allocate(4);
    sc.read(bb);
    bb.flip();

    ByteBuffer bb2 = ByteBuffer.allocate(bb.getInt());
    sc.read(bb2);
    bb2.flip();
    ByteBufferInputStream in = new ByteBufferInputStream(bb2);

    System.out.println("Attente de certificat de " + sc + "...");
    CertificateFactory cf = CertificateFactory.getInstance("X.509");
    X509Certificate certificate = (X509Certificate) cf.generateCertificate(in);
    if (!this.verifyCertifivate(certificate)) {
      sc.close();
      throw new CertificateException("connexion non authentifiée");
    }

    // System.out.println(certificate);
    String alias = certificate.getSubjectDN().getName();
    ks.setCertificateEntry(alias, certificate);

    OutputStream os = new FileOutputStream(hostName + "/ks_" + hostName + ".cer");
    ks.store(os, password);
    socketChannel.close();

    Certificate[] myCerts = ks.getCertificateChain("CN=" + hostName);
    sc.write(ByteBuffer.wrap("\n-----BEGIN CERTIFICATE-----\n".getBytes()));
    sc.write(ByteBuffer.wrap(Base64.getEncoder().encode(myCerts[0].getEncoded())));
    sc.write(ByteBuffer.wrap("\n-----END CERTIFICATE-----\n".getBytes()));
    System.out.println("Authentifié ! ");
    return alias;
  }

  public void generateSessionKey(String alias, SelectionKey s) throws NoSuchAlgorithmException,
      NoSuchPaddingException, NoSuchProviderException, InvalidKeyException, KeyStoreException,
      IllegalBlockSizeException, BadPaddingException, SignatureException, IOException, UnrecoverableKeyException {

    Security.addProvider(new BouncyCastleProvider());

    SocketChannel sc = (SocketChannel) s.channel();

    System.out.println("Génération de la clé de session pour " + alias + "...");

    Cipher c = Cipher.getInstance("RSA", "BC");
    Signature sign = Signature.getInstance("SHA1withRSA", "BC");

    c.init(Cipher.ENCRYPT_MODE, ks.getCertificate(alias).getPublicKey());
    sign.initSign(key.getPrivate());

    KeyGenerator keyGenerator = KeyGenerator.getInstance("DES", "BC");
    Key sessionKey = keyGenerator.generateKey();

    System.out.println("Clé de session générée :" + sessionKey);
    System.out.println("Cryptage et siganture de la clé avant l envoie...");

    byte[] sKeyBytes = sessionKey.getEncoded();
    byte[] sKeyCrypted = c.doFinal(sKeyBytes);
    sign.update(sKeyCrypted);
    byte[] sKeySigned = sign.sign();

    sessionKeys.put(System.identityHashCode(s), sessionKey);

    ByteBuffer bb = ByteBuffer.allocate(4);
    bb.putInt(sKeySigned.length);
    bb.flip();
    sc.write(bb);
    sc.write(ByteBuffer.wrap(sKeySigned));
    bb.clear();
    bb.putInt(sKeyCrypted.length);
    bb.flip();
    sc.write(bb);
    sc.write(ByteBuffer.wrap(sKeyCrypted));
    System.out.println("Clé de session envoyée ! ");
  }

  private void sendFile(SelectionKey s)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CryptoException {
    SocketChannel sc = (SocketChannel) s.channel();

    Key sessionKey = sessionKeys.get(identityHashCode(s));
    Security.addProvider(new BouncyCastleProvider());
    Cipher c = Cipher.getInstance("DES", "BC");
    c.init(Cipher.DECRYPT_MODE, sessionKey);

    Charset charset = Charset.forName("UTF-8");
    ByteBuffer bb = ByteBuffer.allocate(1024);
    int n = 0;
    if ((n = sc.read(bb)) == -1) {
      throw new ConnectException("connxion perdue au cours de reception");
    }
    bb.flip();
    int lenPath = bb.getInt();
    byte[] cryptedPathBytes = new byte[lenPath];
    bb.get(cryptedPathBytes);
    ByteBuffer pathBytes = ByteBuffer.wrap(c.doFinal(cryptedPathBytes));
    String path = charset.decode(pathBytes).toString();

    System.out.println("Cryptage du fichier " + path + " ...");
    Crypto.encryptDES(hostName + "/" + path, hostName + "/" + path + "_crypted", sessionKey);
    FileInputStream fis = new FileInputStream(hostName + "/" + path + "_crypted");
    bb.clear();
    bb.putInt(fis.available());
    bb.flip();
    sc.write(bb);
    byte[] buf = new byte[1024];
    while ((n = fis.available()) > 0) {
      if (n < 1024)
        buf = new byte[n];
      fis.read(buf);
      sc.write(ByteBuffer.wrap(buf));
    }
    System.out.println("Envoyé ! ");
    fis.close();
    sc.close();
  }

  private void reciveFile(SelectionKey s)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CryptoException {
    SocketChannel sc = (SocketChannel) s.channel();

    Key sessionKey = sessionKeys.get(identityHashCode(s));
    Security.addProvider(new BouncyCastleProvider());
    Cipher c = Cipher.getInstance("DES", "BC");
    c.init(Cipher.DECRYPT_MODE, sessionKey);

    Charset charset = Charset.forName("UTF-8");
    ByteBuffer bb = ByteBuffer.allocate(1024);
    int n = 0;
    if ((n = sc.read(bb)) == -1) {
      throw new ConnectException("connxion perdue au cours de reception");
    }
    bb.flip();
    int lenPath = bb.getInt();

    byte[] cryptedPathBytes = new byte[lenPath];
    bb.get(cryptedPathBytes);

    ByteBuffer pathBytes = ByteBuffer.wrap(c.doFinal(cryptedPathBytes));

    String path = charset.decode(pathBytes).toString();
    int sizeData = bb.getInt();
    bb.compact();

    System.out.println("reception du fichier " + path + " de taille " + sizeData + " octets...");
    FileOutputStream fos = new FileOutputStream(hostName + "/" + path + "_crypted");
    bb.flip();
    byte[] buf = new byte[bb.remaining()];
    bb.get(buf);
    fos.write(buf);
    sizeData -= buf.length;
    while (sizeData > 0) {
      bb.clear();
      n = sc.read(bb);
      if (n > 0) {
        buf = new byte[n];
        bb.flip();
        bb.get(buf);
        fos.write(buf);
        sizeData -= n;
      }
    }
    fos.close();
    System.out.println("Decryptage des données...");
    Crypto.decryptDES(hostName + "/" + path + "_crypted", hostName + "/" + path, sessionKey);
    System.out.println("Reçu ! ");
    sc.close();
  }

  @Override
  public void run() {
    System.out.println("SERVEUR " + hostName + " : OK");
        /*-----CERTIFICATION-----*/
    try {
      registerDemande();
      ks.load(new FileInputStream(hostName + "/ks_" + hostName + ".cer"), password);
      getCertificates("localhost", 5000);
      System.out.println("Nombre de certificat reçus = " + ks.size());
    } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | ClassNotFoundException
        | IOException | CertificateException | KeyStoreException e) {
      System.err.println("Certification échouée: " + e.getMessage());
      System.exit(1);
    }

    ByteBuffer bb = ByteBuffer.allocate(4);
    Set<SelectionKey> keys = null;
    while (!stop) {
      try {
        selector.select();
        keys = selector.selectedKeys();
        keys.forEach(s -> {
          if (s.isValid()) {
            if (s.isAcceptable()) {
              accept();
            }
            if (s.isReadable()) {
							/*-----AUTHENTIFICATION + SESSION-----*/
              if (sessionKeys.get(identityHashCode(s)) == null) {
                String alias = "";
                try {
                  alias = authentificate(s);
                  generateSessionKey(alias, s);
                } catch (CertificateException | KeyStoreException | IOException e) {
                  System.err.println("Authentification de " + s + " échouée: " + e.getMessage());
                  e.printStackTrace();
                } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
                    | NoSuchProviderException | UnrecoverableKeyException
                    | IllegalBlockSizeException | BadPaddingException | SignatureException e) {
                  System.err.println("Ouverture de session " + s + " échouée :  " + e.getMessage());
                  e.printStackTrace();
                }
              } else {
								/*-----COMMUNICATION-----*/
                SocketChannel sc = (SocketChannel) s.channel();
                try {
                  sc.read(bb);
                  bb.flip();
                  int mode = bb.getInt();
                  System.out.println("Mode :" + ((mode == 1) ? "upload" : "download"));
                  bb.clear();
                  if (mode == 1)
                    reciveFile(s);
                  else if (mode == 0)
                    sendFile(s);
                  bb.clear();
                  sessionKeys.remove(identityHashCode(s));
                } catch (IOException | InvalidKeyException | NoSuchAlgorithmException
                    | NoSuchProviderException | NoSuchPaddingException | IllegalBlockSizeException
                    | BadPaddingException | CryptoException e) {
                  System.err.println("Opération échoué : " + e.getMessage());
                }
              }
            }
          }
        });
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    new Thread(new Server("ettaieb.fouad@gmail.com", "fouad2012", 12345)).start();
  }
}