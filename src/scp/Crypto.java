package scp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Crypto {

  public static boolean verifySHA1withRSA(byte[] signed, byte[] original, PublicKey pKey)
      throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
    Security.addProvider(new BouncyCastleProvider());

    Signature s = Signature.getInstance("SHA1withRSA", "BC");
    s.initVerify(pKey);
    s.update(original);

    return s.verify(signed);
  }

  public static void encryptDES(String fileIn, String fileOut, Key Key)
      throws CryptoException, FileNotFoundException {
    FileInputStream is = null;
    FileOutputStream os = null;

    try {
      is = new FileInputStream(new File(fileIn));
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("Impossible d'ouvrir le fichier " + fileIn);
    }

    try {
      os = new FileOutputStream(new File(fileOut));
      Cipher c = Cipher.getInstance("DES");
      c.init(Cipher.ENCRYPT_MODE, Key);

      byte[] bytes = new byte[1024];
      while (is.available() > 0) {
        if (is.available() < 1024)
          bytes = new byte[is.available()];
        is.read(bytes);
        os.write(c.update(bytes));
      }
      os.write(c.doFinal());

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
        | BadPaddingException | IOException e1) {
      e1.printStackTrace();
      throw new CryptoException("Cryptage" + fileIn + "impossible :" + e1.getMessage());
    } finally {
      try {
        is.close();
        os.close();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
  }

  public static void decryptDES(String fileIn, String fileOut, Key key) throws CryptoException, FileNotFoundException {
    FileInputStream is = null;
    FileOutputStream os = null;

    try {
      is = new FileInputStream(new File(fileIn));
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("Impossible d'ouvrir le fichier " + fileIn);
    }

    try {
      os = new FileOutputStream(new File(fileOut));

      Cipher c = Cipher.getInstance("DES");

      c.init(Cipher.DECRYPT_MODE, key);
      byte[] bytes = new byte[1024];
      while (is.available() > 0) {
        if (is.available() < 1024)
          bytes = new byte[is.available()];
        is.read(bytes);
        os.write(c.update(bytes));
      }
      os.write(c.doFinal());

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
        | BadPaddingException | IOException e) {
      throw new CryptoException("decryptage" + fileIn + "impossible :" + e.getMessage());
    } finally {
      try {
        os.close();
        is.close();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }

  }

}
