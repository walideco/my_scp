package scp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Client implements Runnable {

  public final static Object LOCK = new Object();

  private String mode;
  private String hostName;
  private char[] password;
  private SocketChannel socketChannel;
  private KeyStore ks;
  private KeyPair key;
  private Key sessionKey;
  private String server;
  private int port;
  private String path;

  public Client(String hoteName, String pwd, String server, int port, String mode, String path)
      throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException,
      NoSuchProviderException {
    if (!mode.equals("upload") && !mode.equals("download"))
      throw new IllegalArgumentException("mode <" + mode + "> invalide");
    this.path = path;
    this.hostName = hoteName;
    this.password = pwd.toCharArray();
    this.mode = mode;
    this.server = server;
    this.port = port;
    Security.addProvider(new BouncyCastleProvider());
    OutputStream os;
    this.ks = KeyStore.getInstance("JKS");
    InputStream is = new FileInputStream(new File("PublicKS.cer"));
    ks.load(is, "".toCharArray());
    is.close();
    os = new FileOutputStream(hoteName + "/ks_" + hoteName + ".cer");
    ks.store(os, this.password);
    os.close();
    KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA", "BC");
    key = keyGenerator.generateKeyPair();

    sessionKey = null;
  }

  public void registerDemande() throws FileNotFoundException, IOException, NoSuchAlgorithmException,
      NoSuchProviderException, InvalidKeySpecException {
    synchronized (LOCK) {
      System.out.println("Enregistrement de demande de certificat aures du CA...");
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("certRequests.txt")));

      KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
      RSAPublicKeySpec rsaPublicKeySpec = kf.getKeySpec(key.getPublic(), RSAPublicKeySpec.class);

      oos.writeUTF(hostName);
      oos.writeObject(rsaPublicKeySpec.getModulus());
      oos.writeObject(rsaPublicKeySpec.getPublicExponent());

      oos.close();
    }
  }

  public void getCertificates(String adresseCAserver, int CAPortt)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException,
      ClassNotFoundException, CertificateException, KeyStoreException {

    InetSocketAddress is = new InetSocketAddress(adresseCAserver, CAPortt);
    this.socketChannel = SocketChannel.open();
    System.out.println("Attente de certificat...");
    if (!this.socketChannel.connect(is))
      throw new ConnectException("CA non disponnible");

    System.out.println("Réception ... ");
    InputStream in = socketChannel.socket().getInputStream();
    CertificateFactory cf = CertificateFactory.getInstance("X.509");
    boolean hasnextCert = true;
    do {
      try {
        X509Certificate certificate = (X509Certificate) cf.generateCertificate(in);
        // System.out.println(certificate);
        if (!this.verifyCertifivate(certificate))
          throw new CertificateException("Impossible de verifier le certificat");
        if (certificate.getSubjectDN().getName().equals("CN=" + hostName)) {
          Certificate[] tmp = {certificate};
          ks.setKeyEntry(certificate.getSubjectDN().getName(), key.getPrivate(), this.password, tmp);
        } else
          ks.setCertificateEntry(certificate.getSubjectDN().getName(), certificate);
      } catch (CertificateException e) {
        hasnextCert = false;
      }
    } while (hasnextCert);

    OutputStream os = new FileOutputStream(hostName + "/ks_" + hostName + ".cer");
    ks.store(os, password);
    socketChannel.close();
  }

  public boolean verifyCertifivate(Certificate certificate) {
    if (certificate == null)
      return false;
    try {
      Certificate certCA = ks.getCertificate("cacertificate");
      certificate.verify(certCA.getPublicKey());

    } catch (KeyStoreException | InvalidKeyException | CertificateException | NoSuchAlgorithmException
        | NoSuchProviderException | SignatureException e) {
      return false;
    }
    return true;
  }

  public String connect(String serverAddr, int port) throws IOException, UnrecoverableKeyException, KeyStoreException,
      NoSuchAlgorithmException, CertificateException {
    InetSocketAddress is = new InetSocketAddress(serverAddr, port);
    socketChannel = SocketChannel.open();

    System.out.println("Connxion à" + serverAddr + "::" + port + "...");
    if (!this.socketChannel.connect(is))
      throw new ConnectException("Serveur " + serverAddr + "::" + port + " non disponnible");

    Certificate[] myCerts = ks.getCertificateChain("CN=" + hostName);

    ByteBuffer b1 = ByteBuffer.wrap("\n-----BEGIN CERTIFICATE-----\n".getBytes());
    ByteBuffer b2 = ByteBuffer.wrap(Base64.getEncoder().encode(myCerts[0].getEncoded()));
    ByteBuffer b3 = ByteBuffer.wrap("\n-----END CERTIFICATE-----".getBytes());

    ByteBuffer bb = ByteBuffer.allocate(4);
    bb.putInt(b1.capacity() + b2.capacity() + b3.capacity());
    bb.flip();
    socketChannel.write(bb);
    socketChannel.write(b1);
    socketChannel.write(b2);
    socketChannel.write(b3);

    InputStream in = socketChannel.socket().getInputStream();
    System.out.println("Réception du certificat de " + socketChannel.getLocalAddress() + "... ");
    CertificateFactory cf = CertificateFactory.getInstance("X.509");
    X509Certificate certificate = (X509Certificate) cf.generateCertificate(in);
    if (!this.verifyCertifivate(certificate)) {
      socketChannel.close();
      throw new CertificateException("connexion non authentifiée");
    }
    String alias = certificate.getSubjectDN().getName();
    ks.setCertificateEntry(alias, certificate);
    System.out.println("Authentifié ! ");
    return alias;
  }

  private byte[] reciveBytes() throws ConnectException, IOException {
    ByteBuffer bb = ByteBuffer.allocate(4);
    if (socketChannel.read(bb) == -1)
      throw new ConnectException("connexion perdue !");
    bb.flip();
    int size = bb.getInt();
    bb.clear();
    InputStream is = socketChannel.socket().getInputStream();
    byte[] bytes = new byte[size];
    if (is.read(bytes) < size)
      return null;
    return bytes;
  }

  public void reciveSessionKey(String alias) throws IOException, NoSuchAlgorithmException, NoSuchProviderException,
      KeyStoreException, InvalidKeyException, SignatureException, CertificateException, NoSuchPaddingException,
      IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnrecoverableKeyException {

    byte[] signedKey = reciveBytes();
    byte[] cryptedKey = reciveBytes();

    PublicKey serverPubKey = ks.getCertificate(alias).getPublicKey();

    if (!Crypto.verifySHA1withRSA(signedKey, cryptedKey, serverPubKey)) {
      socketChannel.close();
      throw new CertificateException("Clé de session : Signature invalide");
    }

    Cipher c = Cipher.getInstance("RSA", "BC");
    Key pk = ks.getKey("CN=" + hostName, password);
    c.init(Cipher.DECRYPT_MODE, pk);
    byte[] decryptedKey = c.doFinal(cryptedKey);
    DESKeySpec kSpec = new DESKeySpec(decryptedKey);
    SecretKeyFactory sk = SecretKeyFactory.getInstance("DES");
    this.sessionKey = sk.generateSecret(kSpec);
    System.out.println("Clé de session reçu " + sessionKey);
  }

  public void download(String path)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CryptoException {

    Security.addProvider(new BouncyCastleProvider());
    Cipher c = Cipher.getInstance("DES", "BC");
    c.init(Cipher.ENCRYPT_MODE, sessionKey);

    Charset charset = Charset.forName("UTF-8");

    System.out.println("Cryptage du chemin du fichier...");
    ByteBuffer CryptedPathBytes = ByteBuffer.wrap(c.doFinal(charset.encode(path).array()));
    int lenPath = CryptedPathBytes.remaining();

    ByteBuffer header = ByteBuffer.allocate(8 + lenPath);
    header.putInt(0);
    header.putInt(lenPath);
    header.put(CryptedPathBytes);
    header.flip();
    System.out.println("Entete envoyé: " + socketChannel.write(header) + " octets");

    FileOutputStream fos = new FileOutputStream(hostName + "/" + path + "_crypted");
    int n = 0;
    ByteBuffer bb = ByteBuffer.allocate(2048);
    while ((n = socketChannel.read(bb)) <= 0) {
      if (n == -1) {
        fos.close();
        throw new ConnectException("Connxion fermée par le serveur ");
      }
    }
    bb.flip();
    int sizeData = bb.getInt();
    System.out.println("Récéption de données :" + sizeData + " Octets");
    byte[] bytes = new byte[bb.remaining()];
    bb.get(bytes);
    fos.write(bytes);
    sizeData -= bytes.length;
    while (sizeData > 0) {
      bb.clear();
      if ((n = socketChannel.read(bb)) == -1) {
        fos.close();
        throw new ConnectException("Connxion fermée par le serveur");
      }
      bb.flip();
      bytes = new byte[n];
      bb.get(bytes);
      fos.write(bytes);
      sizeData -= n;
    }
    fos.close();
    System.out.println("Decryptage des données...");
    Crypto.decryptDES(hostName + "/" + path + "_crypted", hostName + "/" + path, sessionKey);
    System.out.println("Recu !");
  }

  public void upload(String path)
      throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CryptoException {

    Security.addProvider(new BouncyCastleProvider());
    Cipher c = Cipher.getInstance("DES", "BC");
    c.init(Cipher.ENCRYPT_MODE, sessionKey);
    Charset charset = Charset.forName("UTF-8");

    System.out.println("Cryptage du chemin du fichier...");
    ByteBuffer CryptedPathBytes = ByteBuffer.wrap(c.doFinal(charset.encode(path).array()));
    int lenPath = CryptedPathBytes.remaining();

    System.out.println("Cryptage du fichier " + path + " ...");
    Crypto.encryptDES(hostName + "/" + path, hostName + "/" + path + "_crypted", sessionKey);

    System.out.println("Envoie des données cryptées...");
    FileInputStream in = new FileInputStream(hostName + "/" + path + "_crypted");
    int sizeData = in.available();
    ByteBuffer header = ByteBuffer.allocate(12 + lenPath);
    header.putInt(1);
    header.putInt(lenPath);
    header.put(CryptedPathBytes);
    header.putInt(sizeData);
    header.flip();
    System.out.println("Enoie entete:" + socketChannel.write(header));
    byte[] buff = new byte[2048];
    System.out.println("Envoie de données :" + in.available() + " Octets");
    while (in.available() > 0) {
      if (in.available() < 2048)
        buff = new byte[in.available()];
      in.read(buff);
      socketChannel.write(ByteBuffer.wrap(buff));
    }
    System.out.println("Envoyé !");
    in.close();
  }

  @Override
  public void run() {
    System.out.println("CLIENT " + hostName + " : OK");
        /*-----CERTIFICATION-----*/
    try {
      registerDemande();
      ks.load(new FileInputStream(hostName + "/ks_" + hostName + ".cer"), password);
      getCertificates("localhost", 5000);
      System.out.println("Nombre de certificat reçus = " + ks.size());
    } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | ClassNotFoundException
        | IOException | CertificateException | KeyStoreException e) {
      System.err.println("Certification échouée: " + e.getMessage());
      System.exit(1);
    }

		/*-----AUTHENTIFICATION-----*/
    try {
      String alias = connect(server, port);
      reciveSessionKey(alias);
    } catch (InvalidKeyException | NoSuchProviderException | SignatureException | NoSuchPaddingException
        | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
      System.err.println("Session : " + e.getMessage());
      System.exit(1);
    } catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException | CertificateException
        | IOException e) {
      System.err.println("Authetification échouée: " + e.getMessage());
      System.exit(1);
    }

		/*-----COMMUNICATION-----*/

    System.out.println("Mode : " + mode);
    try {
      if ("upload".equals(mode)) {
        System.out.println("Envoie de " + path);
        upload(this.path);

      } else if ("download".equals(mode))
        download(this.path);
      else
        System.exit(1);
    } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException
        | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | CryptoException e) {
      System.err.println("Opération échouée : " + e.getMessage());
      System.exit(1);
    }
  }

  public static void main(String[] args) {
    // try {
    // new Thread(new Client(args[1], args[2], args[3],
    // Integer.parseInt(args[4]),args[5])).start();
    // } catch (NumberFormatException e) {
    // e.printStackTrace();
    // } catch (KeyStoreException e) {
    // e.printStackTrace();
    // } catch (NoSuchAlgorithmException e) {
    // e.printStackTrace();
    // } catch (CertificateException e) {
    // e.printStackTrace();
    // } catch (NoSuchProviderException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    try {
      new Thread(new Client("ettaieb.walid@gmail.com", "mozart2012", "localhost", 12345, "download", "img3.JPG"))
          .start();
    } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException
        | IOException e) {
      e.printStackTrace();
    }
  }

}
