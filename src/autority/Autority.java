package autority;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;

public class Autority {
  static BigInteger SERIAL = BigInteger.ONE;
  int id;
  char[] password;
  KeyStore ks;

  private static Autority AC = new Autority(0, "CApassword");

  protected Autority(int id, String password) {

    Security.addProvider(new BouncyCastleProvider());
    this.id = id;
    this.password = password.toCharArray();
    OutputStream os;
    try {
      this.ks = KeyStore.getInstance("JKS");
      ks.load(null, null);

    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
      System.err.println("Autority non disponible");
      System.exit(0);
    }
  }

  private void intialize() throws NoSuchAlgorithmException, CertificateException, IOException,
      NoSuchProviderException, InvalidKeyException, IllegalStateException, SignatureException, KeyStoreException {

    // rajout d'auto siganture du AC
    KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA", "BC");
    KeyPair key = keyGenerator.generateKeyPair();

    Date startDate = new Date();
    Date expiryDate = new Date(System.currentTimeMillis() + 604800017); // une
    // semaine
    // plus
    // tard
    BigInteger serialNumber = new BigInteger(64, 1, new Random(100000));

    X509V1CertificateGenerator certGenerator = new X509V1CertificateGenerator();
    X500Principal dnName = new X500Principal("CN=Test CA Certificate");
    certGenerator.setSerialNumber(serialNumber);
    certGenerator.setIssuerDN(dnName);
    certGenerator.setNotBefore(startDate);
    certGenerator.setNotAfter(expiryDate);
    certGenerator.setSubjectDN(dnName); // note: same as issuer
    certGenerator.setPublicKey(key.getPublic());
    certGenerator.setSignatureAlgorithm("SHA1withRSA");
    X509Certificate cert = certGenerator.generate(key.getPrivate(), "BC");


    ks.setCertificateEntry("caCertificate", cert);

    FileOutputStream os;
    os = new FileOutputStream(new File("PublicKS.cer"));
    ks.store(os, "".toCharArray());
    os.close();

    Certificate cetificates[] = {cert};
    ks.setKeyEntry("CAPrivate", key.getPrivate(), password, cetificates);
    os = new FileOutputStream(new File("CAKS.cer"));
    ks.store(os, this.password);
    os.close();
  }

  static Autority getInstance() throws InstantiationException {

    try {
      if (AC.ks.size() == 0)
        AC.intialize();
    } catch (InvalidKeyException | KeyStoreException | NoSuchAlgorithmException | CertificateException
        | NoSuchProviderException | IllegalStateException | SignatureException | IOException e) {
      throw new InstantiationException("AC non disponible");
    }
    return AC;

  }

  X509Certificate createV3Certificate(PublicKey pubKey, String Identiy) throws CertificateParsingException,
      UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateEncodingException,
      InvalidKeyException, IllegalStateException, NoSuchProviderException, SignatureException {

    Date startDate = new Date();
    Date expiryDate = new Date(System.currentTimeMillis() + 604800017);
    BigInteger serialNumber = SERIAL;
    SERIAL = SERIAL.add(BigInteger.ONE);

    PrivateKey caKey = (PrivateKey) ks.getKey("CAPrivate", this.password);
    X509Certificate caCert = (X509Certificate) ks.getCertificate("caCertificate");
    X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
    X500Principal subjectName = new X500Principal("CN=" + Identiy);

    certGen.setSerialNumber(serialNumber);
    certGen.setIssuerDN(caCert.getSubjectX500Principal());
    certGen.setNotBefore(startDate);
    certGen.setNotAfter(expiryDate);
    certGen.setSubjectDN(subjectName);
    certGen.setPublicKey(pubKey);
    certGen.setSignatureAlgorithm("SHA1withRSA");

    certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(caCert));
    certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifier(pubKey.getEncoded()));
    X509Certificate cert = certGen.generate(caKey, "BC");
    return cert;

  }

}
