package autority;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Set;

import scp.Client;

public class CAServer implements Runnable {

  private Autority autotity;
  private ServerSocketChannel socket;
  private Selector selector;
  private boolean stop;

  public CAServer(int port) {
    stop = false;
    try {

      socket = ServerSocketChannel.open();
      socket.bind(new InetSocketAddress(port));
      socket.configureBlocking(false);
      selector = Selector.open();
      socket.register(this.selector, SelectionKey.OP_ACCEPT);
      autotity = Autority.getInstance();
    } catch (IOException e) {
      System.err.println("Impossible de demarrer le serveur");
      System.exit(0);
    } catch (InstantiationException e) {
      System.err.println(e.getMessage());
    }
  }

  public void accept() {
    SocketChannel sc = null;
    try {
      sc = socket.accept();
      if (sc != null) {
        System.out.println("Nouvelle demande de certificat :  " + sc);
        sc.configureBlocking(false);
        sc.register(this.selector, (SelectionKey.OP_WRITE));
      }
    } catch (IOException e) {
      System.err.println("Impossible d'accepter le client");
      e.printStackTrace();
    }
  }

  public void sendCertificates(SelectionKey s) throws IOException, ClassNotFoundException, NoSuchAlgorithmException,
      NoSuchProviderException, InvalidKeySpecException, KeyStoreException, UnrecoverableKeyException,
      InvalidKeyException, IllegalStateException, SignatureException, CertificateException {

    SocketChannel sc = (SocketChannel) s.channel();
    String identity = "";
    BigInteger modulus = null;
    BigInteger publicExp = null;
    synchronized (Client.LOCK) {
      ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("certRequests.txt")));
      identity = ois.readUTF();
      modulus = (BigInteger) ois.readObject();
      publicExp = (BigInteger) ois.readObject();
      ois.close();
    }
    RSAPublicKeySpec ks = new RSAPublicKeySpec(modulus, publicExp);
    KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
    PublicKey pubKey = kf.generatePublic(ks);

    X509Certificate newCert;
    newCert = autotity.createV3Certificate(pubKey, identity);
    autotity.ks.setCertificateEntry(newCert.getSubjectDN().getName(), newCert);

    OutputStream os = new FileOutputStream("CAKS.cer");
    autotity.ks.store(os, autotity.password);
    os.close();

    Enumeration<String> aliases = autotity.ks.aliases();
    String alias = "";
    Certificate cert = null;
    while (aliases.hasMoreElements()) {
      alias = aliases.nextElement();

      if (autotity.ks.isCertificateEntry(alias)) {
        cert = autotity.ks.getCertificate(alias);
        if (!autotity.ks.getCertificateAlias(cert).equals("cacertificate")) {
          sc.write(ByteBuffer.wrap("\n-----BEGIN CERTIFICATE-----\n".getBytes()));
          sc.write(ByteBuffer.wrap(Base64.getEncoder().encode(cert.getEncoded())));
          sc.write(ByteBuffer.wrap("\n-----END CERTIFICATE-----".getBytes()));
        }
      }
    }
    sc.close();
  }

  @Override
  public void run() {
    System.out.println("AUTORITE DE CERTFICATION : OK");
    Set<SelectionKey> keys = null;
    while (!stop) {
      try {
        selector.select();
        keys = selector.selectedKeys();
        keys.forEach(s -> {
          if (s.isValid()) {
            if (s.isAcceptable())
              accept();
            else {
              try {
                sendCertificates(s);
                s.cancel();
              } catch (UnrecoverableKeyException | InvalidKeyException | ClassNotFoundException
                  | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException
                  | KeyStoreException | IllegalStateException | SignatureException | IOException
                  | CertificateException e) {
                System.err.println("Envoie des certificats vers " + s + "a echoué : " + e.getMessage());

              }
            }
          }
        });
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    new Thread(new CAServer(5000)).start();
  }

}
